#! /usr/bin/python
# -*- coding: UTF-8 -*-

import urllib, pycurl, sys, getopt




def help():
    print "Usage : api.py -f <filename> [-l <language> -t <title> -e <expiration> -b -c -n -r]"
   
    print "-f <filename> :		(str) Path to the file that you want to Paste"
    print "-l <language> :		(str) Kind of syntax highlighting (cf GeSHi documentation)"
    print "-t <title> :			(str) The title of your Paste"
    print "-e <expiration> :	(str) Paste's lifetime [1H|1D|1W|2W|1M|1Y|NO] (With H represent Hours, W Weeks, Y Years & boundless for NO)"
    print "-b :					Active the auto-burn. The paste will be automatically destroyed after the first reading"
    print "-c :					If this tag is gived, the paste will be encrypted"
    print "-n :					Delete the line return (usefull if you want to treat the response of this program with another one)"
    print "-r :					Give the 'raw' link to the Paste"

    sys.exit()

def read(path):
	try:
		file = open(str(path), 'r')
		lines = file.readlines()

		file.close()

		return lines
	except:
		print "File not found : " + str(path)
        help()
	

def main(argv):


    # Déclarations des variables et attribution des valeurs par défaut
	titre = 'Sans titre'
	expiration = '1M'
	coloration = 'text'
	protect = 'off'
	encrypt = 'off'
	contenu = ''
	content = ''
	autoburn = 'off'

	ok = 0
	noReturn = 0
	raw = 0

	
	try:
	    opts, args = getopt.getopt(argv,"hbcnrf:l:e:t:",['help', 'file=','autoburn','no-return', 'raw', 'encrypt', 'language=', 'expiration=', 'title='])
	except getopt.GetoptError:

		help()

	for opt, arg in opts:
		if opt == '-h' or opt=='--help':
			help()
		elif opt == '-f' or opt=='--file':
			contenu = read(arg)
			ok = 1
		elif opt == '-n' or opt=='--no-return':
			noReturn = 1
		elif opt == '-r' or opt=='--raw':
			raw = 1
		elif opt == '-b' or opt=='--autoburn':
			autoburn = 'on'
		elif opt == '-c' or opt=='--encrypt':
			encrypt = 'on'
		elif opt == '-l' or opt=='--language':
			coloration = arg
		elif opt == '-e' or opt=='--expiration':
			expiration = str(arg)
		elif opt == '-t' or opt=='--title':
			titre = str(arg)

	if ok == 0:
		print 'A file is required'
		help()

	for i in range(len(contenu)) :
		content += str(contenu[i])


	# Fields 
	pf = {
		'titre':titre, 
		'expiration':expiration,
		'auto-burn':autoburn,
		'coloration':coloration,
		'protect':protect,
		'encrypt':encrypt,
		'contenu':content,

		'api':'true',
		'raw':raw
		}

	c = pycurl.Curl()
        c.setopt(c.URL, "http://paste.popoliito.tk/post.php")
	c.setopt(c.POSTFIELDS, urllib.urlencode(pf))
	c.setopt(c.VERBOSE,  0)
	c.perform()
	if noReturn == 0:
		print '\n'
	c.close()

if __name__ == "__main__":
   main(sys.argv[1:])

