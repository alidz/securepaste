<?php
session_start();

require_once('phpseclib/Crypt/AES.php');
require_once('geshi/geshi.php');



	

$error = array();

if(!empty($_GET['id']))
{
	if(preg_match("#[\w]{10}#i", $_GET['id']))
	{
		$fichiers = scandir('data/');
		foreach($fichiers as &$x) // on parcourt tous les noms de fichiers voir si le fichier portant l'id indiqué existe
		{
			if(preg_match("#^".$_GET['id']."-([\d]*)$#i", $x, $matches))
			{
				$name = $matches[0];
				$expiration = $matches[1];
				$find = true;
				break;
			}
			else
				$find = false;

		}

		if($find)
		{
			$Finfo = file_get_contents("data/".$name.".txt");
			$Fdata = file_get_contents("data/".$name);

			if($Fdata && $Finfo)
			{
				$infos = json_decode($Finfo, true);
				/*
				$infos['protect']['type'] -> ip | off | pass
				$infos['protect'][0] -\
				$infos['protect'][1] -- -> les ips autorisées
				$infos['protect'][n] -/
				$infos['protect']['pass'] -> mot de passe

				$infos['langage']
				*/
				$allow = false;

				if($infos['protect']['type'] == "ip")
				{
					$count = count($infos['protect'])-1;

					for($i=0;$i<$count;$i++)
					{
						if(hash('sha256', $_SERVER['REMOTE_ADDR']) == $infos['protect'][$i])
						{
							$allow = true;
							break;
						}
						
					}
				}
				elseif($infos['protect']['type'] == "pass") 
				{
					if(isset($_SESSION['token2']) && isset($_POST['token2']))
					{
						if($_POST['token2'] == $_SESSION['token2'])
						{
							if(isset($_POST['password']) && hash('sha256', $_POST['password']) == $infos['protect']['pass'])
							{
								$allow = true;
								$_SESSION['allow'] = htmlspecialchars($_GET['id']);
							}
							else
								$error[] = "Mot de passe invalide";
							//echo sha1($_POST['password']).' - '.sha1('lolilol').' - '.sha1($infos['protect']['pass']);
						}
						else
							$error[] = "Token invalide.";

						unset($_SESSION['token2']);
					}
					
				}
				else
					$allow = true;


				if(isset($_SESSION['allow']) && $_SESSION['allow'] == $_GET['id'])
				{
					$allow = true;
					foreach($error as &$a)
						$a = '';
				}

				if($allow)
				{	
					$crypt = false;
					if(isset($_GET['key']))
					{
						$crypt = preg_match("#[\w]*#i", $_GET['key']);
						if($crypt)
						{
							$cipher = new Crypt_AES(); // could use CRYPT_AES_MODE_CBC
							// keys are null-padded to the closest valid size
							// longer than the longest key and it's truncated
							//$cipher->setKeyLength(256);
							$key = htmlspecialchars($_GET['key']);
							$cipher->setKey($key);
							//$cipher->setIV('...'); // defaults to all-NULLs if not explicitely defined

							$plaintext = $cipher->decrypt($Fdata);
							$plaintext = trim($plaintext);
						}
						else
							$error[] = "Clef de déchiffrement invalide.";
					}
					else
						$plaintext = trim($Fdata);
					
					if(!empty($plaintext) && empty($error))
					{
						$geshi = new GeSHi($plaintext, $infos['langage']);
		    			$geshi->enable_line_numbers(GESHI_FANCY_LINE_NUMBERS);
					}
					else
						$error[] = "La clef fournie n'a pas permis de déchiffrer correctement le Paste.";
					
				}
				else
					$error[] = "Vous n'êtes pas autorisé à accéder à ce Paste.";
			}
			else
				$error[] = "Impossible d'ouvrir le Paste";
		}
		else
			$error[] = "Le Paste n'existe pas ou plus.";
	}
	else
		$error[] = "ID du Paste invalide.";
}
else
	$error[] = "Requête invalide";

if(empty($error) && !isset($_GET['raw']))
{
	$title="securePaste - ".$infos['titre'];
	include('inc/header.php');
	
	if($expiration == "1")
	{
		?>
		    <div class="alert alert-block">
		    <button type="button" class="close" data-dismiss="alert">&times;</button>
		    <h4>Important !</h4>
		    <p>Ce Paste a été créé pour n'être lu qu'une seule fois. Vous ne pouvez donc ni recharger la page, ni conserver le lien pour y accéder plus tard sans en perdre le contenu.<br />
		    	<span class="label label-warning">Important !</span> Il en va de même pour l'option <i>Raw Paste</i></p>
		    </div>
		<?php
	}
	?>
	<div class="span11" id="main">
		<span style="float:right;display:block;">
			<a href="<?php echo getUrl($crypt, $_GET['id'], @$_GET['key']).'/raw'; ?>" class="btn btn-large"><i class="icon-align-justify"></i> Raw Paste</a> <a href="<?php echo WEBSITE; ?>" class="btn btn-large"><i class="icon-pencil"></i> Créer un nouveau Paste</a>
		</span>
		<h3><?php echo $infos['titre'].' - #'.htmlspecialchars($_GET['id']); ?></h3>
		
	<?php echo $geshi->parse_code(); ?>
	</div>
	<?php
	include('inc/footer.php');
	
	if($expiration == "1")
	{
		unlink('data/'.$name);
		unlink('data/'.$name.'.txt');
	}
		
}
elseif(empty($error) && isset($_GET['raw']))
{
	header('Content-Type: text/plain; charset=UTF-8;');
	echo $plaintext;
}
else
{
	$title= "Fuuuu !";
	include('inc/header.php');
	?>



	<div class="alert alert-error">
    <p><b>Fuuuu !</b><br />
    	Des erreurs ont été rencontrées :
    	<ul>
			<?php
			foreach ($error as &$fu) 
			{
				echo "<li>".$fu."</li>";
			}
			?>
		</ul>
		</p>
    </div>
    <?php
    if(isset($infos) && $infos['protect']['type'] == "pass")
    {
    	$_SESSION['token2'] = md5(uniqid());
    	// echo hash('sha256', $_POST['password']).' - '.$infos['protect']['pass'];
    	?>
	    
		    <div class="alert">
		    	<span class="label label-warning">Important</span> Le créateur de ce Paste a souhaité en restreindre l'accès via mot de passe. 
		    
				<form style="display:block;margin-top:30px;"method="POST" action="<?php echo getUrl($crypt, $_GET['id'], @$_GET['key']);; ?>" class="form-inline">
					<input type="password" name="password" class="input-xlarge" placeholder="Mot de passe">
					<input type="hidden" name="token2" value="<?php echo $_SESSION['token2']; ?>">
					<button type="submit" class="btn">Valider</button>
				</form>
			</div>
	<?php
	}

    echo '<a href="'.WEBSITE.'" class="btn btn-large"><i class="icon-pencil"></i> Créer un nouveau Paste</a>';
	

	include('inc/footer.php');
}