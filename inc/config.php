<?php
// The website's url
define('WEBSITE', 'http://127.0.0.1/');

// To enable or disable the captcha  (incompatible with the API)
define('CAPTCHA', FALSE);

// To enable or disable the token security (incompatible with the API)
define('TOKEN', FALSE);

function getUrl($crypted, $id, $key="")
{
	if(!empty($key))
		$url = WEBSITE. htmlspecialchars($id).'/'.htmlspecialchars($key);
	else
		$url = WEBSITE. htmlspecialchars($id);

	return $url;
}

